# Todo
I can keep coming up with small things I want to either add or change. This is where I just note down the thoughts in case I have time in the future to improve on the project.

### High priority
- Go over the used data types as they can definitely be optimized and aligned a lot better
- Add a way to reset the bedbugs preferences and network settings

### Low priority
- The "successfully saved" checkmark on the settings page looks very out of place on iPhone
- Add a /ping endpoint and have the settings UI verify the target to see if the BedBug can reach it
- Change params for draw API to be form fields. Unable to find examples on it.
