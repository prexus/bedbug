# BedBug

## How we ended up with BedBugs
When I was little I had a piece of string from my bed to my mothers bed. I would occationally pull it and she'd pull back so I knew she was there. That was all I needed to sleep well.

<div align="center">
    <br>
    <img src="./assets/two-way-communication.gif">
    <br>
    <small>
        Two BedBugs exchanging images over wifi
    </small>
    <br>
    <br>
</div>

The technology has now gone wireless with the invention of the BedBug (one that doesn't bite!). It links up to another BedBug over wifi and the two can exchange simple images.

## Hardware & software
This project is developed for the [M5Stack ATOM Matrix](https://docs.m5stack.com/en/core/atom_matrix) and features a 5x5 pixel LED screen that works both as a night light and as a button for cycling through images. Optional accessory is the [M5Stack Buzzer Unit](https://docs.m5stack.com/en/unit/buzzer) if you want your BedBug to give out an audible alarm when it receives a message.

Just for good measure, I am in no way affiliated with M5Stack. I just like using the products :)

The project is setup using PlatformIO.

## Starting the BedBug for the first time
When the BedBug first starts it will show a W on the screen. This means that it is not connected to wifi and it has started it's own. Connect to the wifi named 'BEDBUG-WIFI' using your phone or laptop and visit `http://192.168.4.1` in the browser. Select your home wifi from the dropdown or have it connect to a custom SSID, put in the password and give a hostname for the BedBug. Save the settings and the BedBug will restart.

When the BedBug is restarted and connected to your home wifi it will by default show a nightlight in the display. This is the BedBugs idle screen doubling as a night light. It is configurable on the settings page if you want to change what is displayed or want to lower the intensity of the LEDs.

## Configuring the BedBug
Go back to your browser and make sure your phone or laptop is on the same network as the BedBug. Access the BedBug using the hostname you provided when you configured the wifi or use the assigned IP-address (You can probably see that in your routers DHCP table).

<div align="center">
    <br>
    <a href="./assets/ui.jpg">
        <img src="./assets/ui_small.jpg">
    </a>
    <br>
    <br>
</div>

### General settings
On the configuration page you can see the wifi you are connected to including its signal strength.

#### TARGET
Add the hostname for the target BedBug (the other end of the string). Depending on your home network, this could be a little different - but generally you will use the hostname that you configured the target BedBug with, appended with either `.local` or `.localdomain`. It could look something like this: `bedbug-parent.local`.

#### INCOMING
These options applies on all incoming messages:

**Automatic dismiss:** Will clear the screen 5 seconds after an image was received.<br>
**Click to dismiss:** Will leave the received image on the screen until it's pressed.

<div align="center">
    <br>
    <img src="./assets/click-to-dismiss.gif">
    <br>
    <small>
        With 'Click to dismiss' the image stays until next click
    </small>
    <br>
</div>

#### HEARTS
These options only applies for when the BedBug receives a heart:

**No action:** Nothing special...<br>
**Autorespond w/heart:** If a heart is send to the BedBug it will reply with a heart after 5-10 second and not display it on the screen. If two BedBugs are configured to autorespond with hearts to eachother, the second autoresponse will be cancelled by the originator.

<div align="center">
    <br>
    <img src="./assets/autoreply-hearts-and-idle-screen.gif">
    <br>
    <small>
        Configuration: 'Autorespond w/hearts' and 'Heart counter'
    </small>
    <br>
    <br>
</div>

#### BUZZER
If you have a [M5Stack Buzzer Unit](https://docs.m5stack.com/en/unit/buzzer) you can plug it in the port located on the bottom of your BedBug. You can then use these options to pick a song that plays every time your BedBug receives a message.

Two things to note:
- "Autorespond w/heart" will disable the buzzer in then event of a heart message.
- "Click to dismiss" will cause the song to loop every 3 seconds until you click the screen.

If you select a song from the list, you will hear it when pressing 'Save settings'.

### Idle settings
The default idle settings shows a nightlight on the screen. While it works great as a night light for my kids, the grownups are less enthusiastic about the bedroom being lit up in blue.

#### MODE
Change the look of the idle screen:

**Nightlight:** A blue lampshade with a dim light for a little nightlight.<br>
**Heart counter:** Using red dots to shows the number of automatic heart replies that was made while idling (max 5). If no replies has been made, a blue dot is shown.<br>
**Dot:** A blue dot in the center of the screen<br>
**Always off:** Screen will appear off until an image is received or the screen is pressed to cycle through the images to send.

#### INTENSITY
Apart from the _Always off_ mode it might be useful to dim the intensity or brightness of the lights a bit. Select the light levels you are comfortable with in your bedroom.

<div align="center">
    <br>
    <img src="./assets/brightness-settings.gif">
    <br>
    <small>
        Different intensity levels for 'Nightlight' and 'Dot' idle modes
    </small>
    <br>
</div>

### Click cycle
Here you configure the images that can be send to the target BedBug by pressing the screen. You can configure from 1 to 5 screens.

When you are happy with the configuration, save your settings.

## Using the BedBug
It could hardly be any easier to use the BedBug. Press the screen to cycle through the images you configured in the settings interface.

Stop on the image you want to send to the target BedBug and after 3 seconds a loading animation will appear while it's transferring the image. If you reach the end of the cycle, it will go back to the idle screen.

After a short time you will get a checkmark if the message was delivered - alternatively a red cross if the target BedBug could not be reached.

In case it fails, check your network settings and wifi coverage where you installed the BedBug.

Remember that the antennas in these small devices are not the same as your phone or laptop. You can check the connected wifi and its signal strength on the setting page.

## Going online (Advanced!)
The BedBug is meant for your home network, but that doesn't mean you can't take it a step further. If you know how to configure NAT in your router and you have an external IP address you can set two BedBugs up to point at eachother over the internet too - so technically no limit to the length of the string :)

Be aware that opening up your home network to the web could introduce some security challenges, so be aware of the risks.

## Integrating with the BedBug API
In case you want to integrate the BedBug with something different than another BedBug - then these endpoints that could be interesting to you:

### List images
List all the images and their corresponding ID:

```
GET /images               200
```
Use the IDs from this endpoint to draw to the screen with the draw endpoint.

### Draw image
Draws an image on the screen. This automatically respects the BedBugs setting for automatic or click dismissal.
```
POST /draw?id=<imageid>   204
```
There is the optional parameter `ignoreAutoReply` that you can set to `true` if you want to make sure that, in case the BedBug is configured to autorespond to hearts - it will not do that for this draw request.

If you send a heart and the BedBug is setup to autorespond with a heart to its target, you will not see an image on the screen, but instead the BedBug will trigger the targets draw endpoint with a heart instead.

I would like to move the query parameters to form fields since it is a POST, but I have not cracked the code on how to do that with the library I use to do the asynchronous http requests. You are more than welcome to reach out if you can help me!

<hr>

## Development details

### Classes vs namespaces
I was not sure what the correct approach was in regards to using classes with static methods vs namespaces when it comes to things like settings, screen, etc.. I did some research and also took the question to the "Together C & C++" discord (https://discord.gg/WFPthMbe) to see what they thought. That spawned a fruitful discussion and it kinda split the channel into two camps.

 - Camp 1: Use namespaces if you don't need instantiated variables
 - Camp 2: Use classes because you might need to add instantiation later

 While both have good points, the reason I chose to use classes over namespaces ended up being that I feel it's easier to get an overview of private and public methods in classes vs. namespaces where you have to encapsulate your private methods in an anonymous namespace in the beginning of the file.

 Private methods are often called later than the public ones, so it bugged me that these methods were now places on top and I couldn't store them away in the bottom where I feel was a more rightful place for them.

 That's how I decided to use classes over namespaces.

### Upload UI

The UI to configure the BedBug needs to be uploaded to the filesystem. If you use PlatformIO you can upload it using:

```bash
$ pio run -t uploadfs
```

Please note that this formats the filesyste and will clear your credentials set using the Wifi Manager.
