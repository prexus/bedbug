let settings = null;
let images = null;
let songs = null;

function initialize() {
    getStatus();
}

function getStatus() {
    const xhr = new XMLHttpRequest();

    xhr.addEventListener("load", () => onStatusLoaded(xhr.responseText));
    xhr.open('GET', '/status');
    xhr.send();
}

function onStatusLoaded(response) {
    const status = JSON.parse(response);

    const span = document.getElementById("status");
    span.innerHTML = 'Connected to ' + status.ssid + ' (' + status.rssi + ' dBm)';

    getSettings();
}

function getSettings() {
    const xhr = new XMLHttpRequest();

    xhr.addEventListener("load", () => onSettingsLoaded(xhr.responseText));
    xhr.open('GET', '/settings');
    xhr.send();
}

function onSettingsLoaded(response) {
    settings = JSON.parse(response);

    getOptions();
}

function getOptions() {
    const xhr = new XMLHttpRequest();

    xhr.addEventListener("load", () => onOptionsLoaded(xhr.responseText));
    xhr.open('GET', '/options');
    xhr.send();
}

function onOptionsLoaded(data) {
    const options = JSON.parse(data)

    images = options.images;
    songs = options.songs;

    onDataLoaded();
}

function onDataLoaded() {
    const elementIds = [
        "target",
        "clickToDismiss",
        "autoRespondToHearts",
        "idleScreenMode",
        "idleScreenBrightness"
    ];

    for (const elementId of elementIds) {
        const element = document.getElementById(elementId);
        element.value = settings[elementId];
    }

    drawClickOptions();
    addBuzzerSongs();
    showInterface();
}

function drawClickOptions() {
    for (const imageId of settings.screens) {
        addClickOption(imageId);
    }

    const canAddScreens = settings.screens.length < 5;

    const add = document.getElementById("add");
    add.style.display = canAddScreens ? 'block' : 'none';
}

function redrawClickOptions() {
    const container = document.getElementById("screens");
    container.replaceChildren();

    drawClickOptions();
}

function addClickOption(imageId) {
    const container = document.getElementById("screens");
    const index = container.childNodes.length;

    const label = document.createElement("label");
    label.for = "screen" + index;
    label.innerHTML = 'Click #' + (index + 1);

    const select = document.createElement("select");
    select.id = "screen" + index;
    select.onchange = () => onScreenSelectionChanged(index, select.value);

    for (const image of images) {
        const option = document.createElement("option");
        option.value = image.id;
        option.text = image.name;
        option.selected = image.id === imageId;

        select.appendChild(option);
    }

    const span = document.createElement("span");
    span.appendChild(label);
    span.appendChild(select);

    // Delete button is only added if we have more
    // than one screen, just to make sure the list
    // looks nice - plus it does not make sense to
    // delete all screens in my opionion.
    if (settings.screens.length > 1) {
        const remove = document.createElement("a");
        remove.onclick = () => removeClickOption(index);
        remove.className = "remove";
        remove.innerHTML = "X";

        span.appendChild(remove);
    }

    container.appendChild(span);
}

function removeClickOption(index) {
    settings.screens.splice(index, 1);

    redrawClickOptions();
}

function addNewScreen() {
    settings.screens.push(0);

    redrawClickOptions();
}

function onScreenSelectionChanged(index, value) {
    settings.screens[index] = parseInt(value);
}

function addBuzzerSongs() {
    const select = document.getElementById("buzzerSongId");

    for (const song of songs) {
        const option = document.createElement("option");
        option.value = song.id;
        option.text = song.name;
        option.selected = song.id.toString() === settings.buzzerSongId;

        select.appendChild(option);
    }
}

function showInterface() {
    const loader = document.getElementById("large-loader");
    loader.style.display = 'none';

    const wrapper = document.getElementById("wrapper");
    wrapper.style.display = 'flex';
}

function saveSettings() {
    // In case of a 2nd save action, we need to hide the confirmation again
    const confirmation = document.getElementById('confirmation');
    confirmation.style.display = 'none';

    const loader = document.getElementById('small-loader');
    loader.style.display = 'inline';

    const saveButton = document.getElementById('save');
    saveButton.setAttribute('disabled', '');

    const xhr = new XMLHttpRequest();

    xhr.addEventListener("load", () => onRequestLoad(xhr.status));
    xhr.addEventListener("error", onRequestError);
    xhr.addEventListener("loadend", onRequestEnded);

    xhr.open('POST', '/settings');
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    // Delaying the call a little bit to give a better experience with
    // the disappearing confirmation checkmark (on 2nd save) and getting
    // to see the loading spinner for more than 10ms
    setTimeout(() => { xhr.send(JSON.stringify(settings)); }, 500);
}

function onRequestLoad(status) {
    if (status !== 204) {
        onRequestError(null, status);
    }

    const confirmation = document.getElementById('confirmation');
    confirmation.style.display = 'inline';
}

function onRequestError(event, status) {
    let error = 'Unable to save settings';

    if (status) {
        error += '\n\nServer responded with: ' + status;
    }

    // Gives onRequestEnded() a chance to run first
    setTimeout(() => alert(error), 100);
}

function onRequestEnded() {
    const loader = document.getElementById('small-loader');
    loader.style.display = 'none';

    const saveButton = document.getElementById('save');
    saveButton.removeAttribute('disabled');
}
