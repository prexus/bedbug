#include "HttpEndpoint.h"

#include "Buzzer.h"
#include "Images.h"
#include "State.h"
#include "Screen.h"
#include "Settings.h"
#include "Timers.h"

#include <ESPAsyncWebServer.h>
#include <AsyncJson.h>
#include <ArduinoJson.h>
#include <SPIFFS.h>
#include <WiFi.h>

AsyncWebServer HttpEndpoint::server(80);

void HttpEndpoint::start() {
    // TODO: Remove when done, this enables CORS
    DefaultHeaders::Instance().addHeader("Access-Control-Allow-Origin", "*");
    DefaultHeaders::Instance().addHeader("Access-Control-Allow-Headers", "content-type");

    server
        .serveStatic("/", SPIFFS, "/bedbug-client")
        .setDefaultFile("index.html");

    server.on("/draw", HTTP_POST, onDrawRequest);
    server.on("/status", HTTP_GET, onStatusRequest);
    server.on("/options", HTTP_GET, onOptionsRequest);
    server.on("/settings", HTTP_GET, onSettingsRequest);

    auto handler = new AsyncCallbackJsonWebHandler("/settings", onSaveSettingsRequest);
    server.addHandler(handler);

    server.begin();
}

void HttpEndpoint::onDrawRequest(AsyncWebServerRequest *request) {
    AsyncWebParameter *id = request->getParam("id");
    AsyncWebParameter *ignoreAutoReply = request->getParam("ignoreAutoReply");

    if (id == NULL) {
        Serial.println("No id provided");

        request->send(400);

        return;
    }

    String value = id->value();
    int imageId = value.toInt();

    if (imageId == 0) {
        Serial.printf("Received invalid draw request (%s -> %d)\n", value.c_str(), imageId);

        request->send(400);

        return;
    }

    auto image = Images::getImage(imageId);

    if (image == NULL) {
        Serial.printf("Received invalid draw request index %d\n", imageId);

        request->send(404);

        return;
    }

    Serial.printf("Received request to draw index %d\n", imageId);

    request->send(204);

    // Figure out if we can autoreply
    bool allowAutoReply = true;

    if (ignoreAutoReply != NULL && ignoreAutoReply->value() == "true") {
        allowAutoReply = false;
    }

    // If autoRespondToHearts is enabled and we received a HEART then we
    // will start the heartTimer without drawing the image to the screen.
    if (Settings::autoRespondToHearts() && image->type == ImageType::HEART) {
        if (allowAutoReply) {
            // Update screen with new heart count
            State::heartCounter++;
            Screen::update();

            // Relatively random no between 5k and 10k
            int timeout = 5000 + ( std::rand() % ( 10000 - 5000 + 1 ) );

            Timers::setHeartTimer(timeout);
        }

        return;
    }

    Screen::draw(image->type);

    State::clearOnNextClick = true;

    if (!Settings::clickToDismiss()) {
        Timers::setResetTimer(5000);
    }

    if (Settings::buzzerSongId() > 0) {
        auto songId = Settings::buzzerSongId();
        auto loop = Settings::clickToDismiss();

        Buzzer::play(songId, loop);
    }
}

void HttpEndpoint::onStatusRequest(AsyncWebServerRequest *request) {
    // These come from the wifi manager and since we
    // are connected to the wifi, the files must be there.
    // Since I made thw wifi manager myself, maybe I should
    // add a feature where I could pull this through a
    // static method on the WifiManager class.
    SPIFFS.begin(true);
    File file = SPIFFS.open("/ssid.txt");
    String ssid = file.readStringUntil('\n');
    file.close();
    // - - - - - -

    StaticJsonDocument<1024> root;

    root["rssi"] = WiFi.RSSI();
    root["ssid"] = ssid;

    String output;

    serializeJson(root, output);

    request->send(200, "application/json", output);
}

void HttpEndpoint::onOptionsRequest(AsyncWebServerRequest *request) {
    StaticJsonDocument<1024> root;

    auto images = root.createNestedArray("images");
    auto songs = root.createNestedArray("songs");

    for (int i = 0; i < Images::getImageCount(); i++) {
        auto image = Images::getImageByIndex(i);

        if (image->selectable) {
            auto entry = images.createNestedObject();

            entry["id"] = image->id;
            entry["name"] =image->label;
        }
    }

    for (int i = 0; i < Songs::getSongCount(); i++) {
        auto song = Songs::getSongByIndex(i);

        auto entry = songs.createNestedObject();

        entry["id"] = song->id;
        entry["name"] =song->label;
    }

    String output;

    serializeJson(root, output);

    request->send(200, "application/json", output);
}

void HttpEndpoint::onSaveSettingsRequest(AsyncWebServerRequest *request, JsonVariant &body) {
    // Obviously a nasty security flaw right here as it'll just
    // store whatever it gets from the client. ArduinoJson does
    // not have any plans for supporting schema validation, so
    // this has to be dealt with in some other way.
    JsonObject jsonObject = body.as<JsonObject>();

    JsonArray screens = jsonObject["screens"];

    if (screens.size() == 0 || screens.size() > 5) {
        request->send(400);

        Screen::splash(ImageType::ERROR);

        return;
    }

    Settings::save(jsonObject);

    request->send(204);

    if (Settings::buzzerSongId() > 0) {
        Buzzer::play(Settings::buzzerSongId());
    }

    Screen::splash(ImageType::CHECKMARK);
}

void HttpEndpoint::onSettingsRequest(AsyncWebServerRequest *request) {
    String json = Settings::getSettings();

    request->send(200, "application/json", json);
}
