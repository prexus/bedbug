#pragma once

enum Note {
    TONE,
    OCTAVE,
    DURATION,
    REST
};

class SongData
{
    public:
        static const int carolOfTheBells[][4];
        static const int fanfare[][4];
        static const int imLovingIt[][4];
        static const int playground[][4];
};
