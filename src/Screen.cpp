#include <M5Atom.h>

#include "Colors.h"
#include "Images.h"
#include "Screen.h"
#include "Settings.h"
#include "State.h"
#include "Timers.h"

int Screen::currentFrame = 0;
bool Screen::idling = true;

void Screen::draw(ImageType type) {
    drawFrame(type, 0, 100);
}

void Screen::draw(ImageType type, int brightness) {
    drawFrame(type, 0, brightness);
}

void Screen::drawFrame(ImageType type) {
    drawFrame(type, currentFrame++, 100);
    delay(200);
}

void Screen::splash(ImageType type) {
    State::clearOnNextClick = true;

    Timers::setResetTimer(3000);

    draw(type);
}

void Screen::idle() {
    String mode = Settings::idleScreenMode();
    int brightness = Settings::idleScreenBrightness();

    if (mode == "off") {
        M5.dis.clear();
    } else if (mode == "dot") {
        draw(ImageType::DOT, brightness);
    } else if (mode == "counter") {
        drawHeartCounter(brightness);
    } else {
        // Default
        draw(ImageType::NIGHTLIGHT, brightness);
    }

    idling = true;
}

void Screen::update() {
    if (!idling) {
        return;
    }

    idle();
}

void Screen::drawHeartCounter(int brightness) {
    if (State::heartCounter == 0) {
        draw(ImageType::DOT, brightness);

        return;
    }

    State::clearOnNextClick = true;

    auto image = Images::getImage(ImageType::COUNTER);
    int maxFrameIndex = image->totalFrames - 1;
    int adjustedCount = State::heartCounter - 1;
    int frameIndex = min(adjustedCount, maxFrameIndex);

    drawFrame(ImageType::COUNTER, frameIndex, brightness);
}

void Screen::drawFrame(ImageType type, int frameCounter, int brightness) {
    auto image = Images::getImage(type);

    if (image) {
        auto frameIndex = frameCounter % image->totalFrames;
        auto frameData = image->frames[frameIndex];

        print(frameData, brightness);
    }
}

void Screen::print(const int * image, int brightness) {
    idling = false;

    for (int i = 0; i < 25; i++) {
        unsigned int hex = image[i];
        signed char pct = brightness;

        if (brightness < 100) {
            hex = Colors::dim(hex, pct);
        }

        M5.dis.drawpix(i, hex);
    }
}

void Screen::printFrame(const int image[][25], int &frameIndex, const int &frameCount) {
    print(image[frameIndex % frameCount], 100);

    frameIndex++;
}
