#pragma once

class State {
    public:
        static int clickCycle;
        static int heartCounter;

        static bool loading;
        static bool success;

        static bool clearOnNextClick;

        static void reset();
};
