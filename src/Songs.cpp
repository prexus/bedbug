#include <Arduino.h>

#include "Songs.h"
#include "SongData.h"

Song Songs::songs[] {
    // ID   LABEL                  DATA
    {  1,   "Carol of the Bells",  SongData::carolOfTheBells },
    {  2,   "Fanfare",             SongData::fanfare         },
    {  3,   "I'm loving it",       SongData::imLovingIt      },
    {  4,   "Playground",          SongData::playground      }
};

int Songs::songCount = sizeof(songs)/sizeof(songs[0]);

int Songs::getSongCount() {
    return songCount;
}

Song* Songs::getSong(int songId) {
    for (int i = 0; i < songCount; i++) {
        if (songs[i].id == songId) {
            return &songs[i];
        }
    }

    return nullptr;
}

Song* Songs::getSongByIndex(int index) {
    return &songs[index];
}
