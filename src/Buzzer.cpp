#include <Arduino.h>

#include "Buzzer.h"
#include "Songs.h"

const int Buzzer::channel = 0;

Song * Buzzer::currentSong = nullptr;
bool Buzzer::looping = false;
bool Buzzer::resting = true;
int Buzzer::noteIndex = 0;
int Buzzer::actionTimer = 0;

void Buzzer::init() {
    int resolution = 10;
    int frequency = 4000;
    int pin = 26;

    ledcSetup(channel, frequency, resolution);
    ledcAttachPin(pin, channel);
}

void Buzzer::play(int songId) {
    play(songId, false);
}

void Buzzer::play(int songId, bool loop) {
    currentSong = Songs::getSong(songId);

    noteIndex = 0;
    resting = true;
    looping = loop;
}

void Buzzer::stop() {
    currentSong = nullptr;
    ledcWrite(channel, 0);
}

void Buzzer::update() {
    if (currentSong == nullptr) {
        return;
    }

    if (actionTimer > millis()) {
        return;
    }

    if (resting) {
        startNote();
    } else {
        stopNote();
    }
}

void Buzzer::startNote() {
    auto note = currentSong->notes[noteIndex];
    auto tone = static_cast<note_t>(note[Note::TONE]);
    auto octave = note[Note::OCTAVE];
    auto duration = note[Note::DURATION];

    ledcWriteNote(channel, tone, octave);

    resting = false;
    actionTimer = millis() + duration;
}

void Buzzer::stopNote() {
    ledcWrite(channel, 0);

    resting = true;

    auto note = currentSong->notes[noteIndex];
    auto rest = note[Note::REST];

    if (rest == -1) {
        lastNoteReached();
    } else {
        readyNextNote(rest);
    }
}

void Buzzer::readyNextNote(int rest) {
    actionTimer = millis() + rest;
    noteIndex++;
}

void Buzzer::lastNoteReached() {
    if (!looping) {
        currentSong = nullptr;

        return;
    }

    // Starts again in 3 seconds
    actionTimer = millis() + 3000;
    noteIndex = 0;
}
