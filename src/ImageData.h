#pragma once

class ImageData
{
    public:
        static const int checkmark[][25];
        static const int counter[][25];
        static const int dot[][25];
        static const int error[][25];
        static const int happy[][25];
        static const int heart[][25];
        static const int indifferent[][25];
        static const int loading[][25];
        static const int nightlight[][25];
        static const int question[][25];
        static const int sad[][25];
        static const int thumbsUp[][25];
        static const int wifi[][25];
};
