#pragma once

#include <Arduino.h>

#include "SongData.h"

struct Song
{
    const int id;
    const char* label;
    const int (*notes)[4];
};

class Songs {
        private:
        static Song songs[];
        static int songCount;

        public:
        static int getSongCount();
        static Song * getSong(int songId);
        static Song * getSongByIndex(int index);
};
