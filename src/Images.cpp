#include "ImageData.h"
#include "Images.h"

const Image Images::images[] {
    // ID  TYPE                    LABEL          DATA                 FRAMES    SELECTABLE
    {  1,  ImageType::CHECKMARK,   "Checkmark",   ImageData::checkmark,   1,     true  },
    {  2,  ImageType::COUNTER,     "Counter",     ImageData::counter,     5,     false },
    {  3,  ImageType::DOT,         "Dot",         ImageData::dot,         1,     false },
    {  4,  ImageType::ERROR,       "Error",       ImageData::error,       1,     false },
    {  5,  ImageType::HAPPY,       "Happy",       ImageData::happy,       1,     true  },
    {  6,  ImageType::HEART,       "Heart",       ImageData::heart,       1,     true  },
    {  7,  ImageType::INDIFFERENT, "Indifferent", ImageData::indifferent, 1,     true  },
    {  8,  ImageType::LOADING,     "Loading",     ImageData::loading,     3,     false },
    {  9,  ImageType::NIGHTLIGHT,  "Nightlight",  ImageData::nightlight,  1,     true  },
    { 10,  ImageType::QUESTION,    "Question",    ImageData::question,    1,     true  },
    { 11,  ImageType::SAD,         "Sad",         ImageData::sad,         1,     true  },
    { 12,  ImageType::THUMBS_UP,   "Thumbs up",   ImageData::thumbsUp,    1,     true  },
    { 13,  ImageType::WIFI,        "Wifi",        ImageData::wifi,        1,     false }
};

const int Images::imageCount = sizeof(images)/sizeof(images[0]);

const int Images::getImageCount() {
    return imageCount;
}

const Image* Images::getImageByIndex(int index) {
    return &images[index];
}

const Image* Images::getImage(int imageId) {
    for (int i = 0; i < imageCount; i++) {
        if (images[i].id == imageId) {
            return &images[i];
        }
    }

    return nullptr;
}

const Image* Images::getImage(ImageType type) {
    for (int i = 0; i < imageCount; i++) {
        if (images[i].type == type) {
            return &images[i];
        }
    }

    return nullptr;
}
