#include "Colors.h"

#include <Arduino.h>

unsigned int Colors::dim(unsigned int& hexIn, signed char& pct) {
    float hue;
    float saturation;
    float value;

    hexToHsv(hexIn, hue, saturation, value);

    value *= pct / 100.0;

    size_t hexOut;

    hsvToHex(hexOut, hue, saturation, value);

    return hexOut;
}

// With inspiration from https://gist.github.com/fairlight1337/4935ae72bcbcc1ba5c72
void Colors::hexToHsv(unsigned int& hex, float& hue, float& saturation, float& value) {
    size_t red = (hex & 0xFF0000) >> 16;
    size_t green = (hex & 0x00FF00) >> 8;
    size_t blue = (hex & 0x0000FF);

    float chromaMax = max(max(red, green), blue);
    float chromaMin = min(min(red, green), blue);
    float fDelta = chromaMax - chromaMin;

    if (fDelta > 0) {
        if (chromaMax == red) {
            hue = 60 * (fmod(((green - blue) / fDelta), 6));
        } else if (chromaMax == green) {
            hue = 60 * (((blue - red) / fDelta) + 2);
        } else if (chromaMax == blue) {
            hue = 60 * (((red - green) / fDelta) + 4);
        }

        if (chromaMax > 0) {
            saturation = fDelta / chromaMax;
        } else {
            saturation = 0;
        }

        value = chromaMax;
    } else {
        hue = 0;
        saturation = 0;
        value = chromaMax;
    }

    if (hue < 0) {
        hue = 360 + hue;
    }
}

// With inspiration from https://gist.github.com/fairlight1337/4935ae72bcbcc1ba5c72
void Colors::hsvToHex(unsigned int& hex, float& hue, float& saturation, float& value) {
    unsigned int red;
    unsigned int green;
    unsigned int blue;

    float chroma = value * saturation;
    float huePrime = fmod(hue / 60.0, 6);
    float fX = chroma * (1 - fabs(fmod(huePrime, 2) - 1));
    float fM = value - chroma;

    if (0 <= huePrime && huePrime < 1) {
        red = chroma;
        green = fX;
        blue = 0;
    } else if (1 <= huePrime && huePrime < 2) {
        red = fX;
        green = chroma;
        blue = 0;
    } else if (2 <= huePrime && huePrime < 3) {
        red = 0;
        green = chroma;
        blue = fX;
    } else if (3 <= huePrime && huePrime < 4) {
        red = 0;
        green = fX;
        blue = chroma;
    } else if (4 <= huePrime && huePrime < 5) {
        red = fX;
        green = 0;
        blue = chroma;
    } else if (5 <= huePrime && huePrime < 6) {
        red = chroma;
        green = 0;
        blue = fX;
    } else {
        red = 0;
        green = 0;
        blue = 0;
    }

    red += fM;
    green += fM;
    blue += fM;

    hex = ((red & 0xff) << 16) + ((green & 0xff) << 8) + (blue & 0xff);
}
