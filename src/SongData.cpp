#include <Arduino.h>

#include "SongData.h"

const int SongData::carolOfTheBells[][4] PROGMEM = {
    { NOTE_C, 7, 300,  50 },
    { NOTE_B, 6, 150,  50 },
    { NOTE_C, 7, 150,  50 },
    { NOTE_A, 6, 300, 100 },
    { NOTE_C, 7, 300,  50 },
    { NOTE_B, 6, 150,  50 },
    { NOTE_C, 7, 150,  50 },
    { NOTE_A, 6, 300,  -1 }
};

const int SongData::fanfare[][4] PROGMEM = {
    { NOTE_A, 5, 300, 50 },
    { NOTE_A, 5,  80, 50 },
    { NOTE_A, 5,  80, 50 },
    { NOTE_E, 6, 500, -1 }
};

const int SongData::imLovingIt[][4] PROGMEM = {
    { NOTE_Bb, 6, 300,  50 },
    { NOTE_Gs, 6, 300,  50 },
    { NOTE_Fs, 6, 120,  80 },
    { NOTE_Fs, 6, 200,  -1 },
};

const int SongData::playground[][4] PROGMEM = {
    { NOTE_G, 6, 300, 50 },
    { NOTE_G, 6,  80, 50 },
    { NOTE_E, 6, 300, 50 },
    { NOTE_A, 6,  80, 50 },
    { NOTE_G, 6, 500, 50 },
    { NOTE_E, 6, 400, -1 }
};
