#include "BedBug.h"
#include "API.h"
#include "Buzzer.h"
#include "Images.h"
#include "State.h"
#include "Screen.h"
#include "HttpEndpoint.h"
#include "Settings.h"
#include "Timers.h"

#include <Arduino.h>
#include <M5Atom.h>

bool BedBug::started = false;

void BedBug::initialize() {
    Screen::draw(ImageType::WIFI);
}

void BedBug::start() {
    Settings::load();
    Screen::idle();
    Buzzer::init();

    HttpEndpoint::start();

    started = true;
}

void BedBug::check() {
    if (!started) {
        return;
    }

    if (M5.Btn.wasPressed()) {
        onButtonPressed();
    }

    if (Timers::triggerSendTimer()) {
        transferSelectedImage();
    }

    if (Timers::triggerHeartTimer()) {
        API::sendImage(ImageType::HEART, RequestType::AUTO_REPLY);
    }

    if (Timers::triggerResetTimer()) {
        State::reset();
    }

    M5.update();

    Buzzer::update();
}

void BedBug::onButtonPressed() {
    if (State::clearOnNextClick) {
        Timers::clearSendTimer();
        Buzzer::stop();
        State::reset();
    } else {
        State::clickCycle++;

        showNextImage();
    }
}

void BedBug::showNextImage() {
    int screensCount = Settings::getScreenCount();
    int maxClickCycle = screensCount + 1;

    if (State::clickCycle == maxClickCycle) {
        State::clickCycle = 0;

        // Goes back to idle and resets timers
        Timers::clearSendTimer();
        Screen::idle();

        return;
    }

    Timers::setSendTimer(3000);

    auto image = getCurrentImage();

    Screen::draw(image);
}

void BedBug::transferSelectedImage() {
    // A little fake loading screen to give
    // more faith in it actually sends.
    initLoadingScreen();

    auto image = getCurrentImage();

    transferImage(image);
}

void BedBug::transferImage(ImageType image) {
    API::sendImage(image);

    showLoadingScreen();

    Screen::splash(
        State::success ?
        ImageType::CHECKMARK :
        ImageType::ERROR
    );
}

ImageType BedBug::getCurrentImage() {
    int screenIndex = State::clickCycle - 1;
    int imageId = Settings::getScreen(screenIndex);
    auto image = Images::getImage(imageId);

    return image->type;
}

void BedBug::initLoadingScreen() {
    for (size_t i = 0; i < 6; i++) {
        Screen::drawFrame(ImageType::LOADING);
    }
}

void BedBug::showLoadingScreen() {
    while (State::loading) {
        Screen::drawFrame(ImageType::LOADING);
    }
}
