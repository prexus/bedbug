#pragma once

#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>

class HttpEndpoint {
    private:
        static AsyncWebServer server;

        static void onDrawRequest(AsyncWebServerRequest *request);
        static void onStatusRequest(AsyncWebServerRequest *request);
        static void onOptionsRequest(AsyncWebServerRequest *request);
        static void onSettingsRequest(AsyncWebServerRequest *request);
        static void onSaveSettingsRequest(AsyncWebServerRequest *request, JsonVariant &body);

    public:
        static void start();
};
