#include <Arduino.h>

#include "ImageData.h"

const int ImageData::checkmark[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x00ff00,
        0x000000, 0x000000, 0x000000, 0x00ff00, 0x000000,
        0x00ff00, 0x000000, 0x00ff00, 0x000000, 0x000000,
        0x000000, 0x00ff00, 0x000000, 0x000000, 0x000000
    }
};

const int ImageData::counter[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0xff0000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000
    },
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0xff0000, 0x000000, 0xff0000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000
    },
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0xff0000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0xff0000, 0x000000, 0xff0000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000
    },
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0xff0000, 0x000000, 0xff0000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0xff0000, 0x000000, 0xff0000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000
    },
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0xff0000, 0x000000, 0x000000,
        0x000000, 0xff0000, 0xff0000, 0xff0000, 0x000000,
        0x000000, 0x000000, 0xff0000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000
    }
};

const int ImageData::dot[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x0000ff, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000
    }
};

const int ImageData::error[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0xff0000, 0x000000, 0xff0000, 0x000000,
        0x000000, 0x000000, 0xff0000, 0x000000, 0x000000,
        0x000000, 0xff0000, 0x000000, 0xff0000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000
    }
};

const int ImageData::happy[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x00ff00, 0x000000, 0x00ff00, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x00ff00, 0x000000, 0x000000, 0x000000, 0x00ff00,
        0x000000, 0x00ff00, 0x00ff00, 0x00ff00, 0x000000,
    }
};

const int ImageData::heart[][25] PROGMEM =
{
    {
        0x000000, 0xff0000, 0x000000, 0xff0000, 0x000000,
        0xff0000, 0xff0000, 0xff0000, 0xff0000, 0xff0000,
        0xff0000, 0xff0000, 0xff0000, 0xff0000, 0xff0000,
        0x000000, 0xff0000, 0xff0000, 0xff0000, 0x000000,
        0x000000, 0x000000, 0xff0000, 0x000000, 0x000000
    }
};

const int ImageData::indifferent[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0xffff00, 0x000000, 0xffff00, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0xffff00, 0xffff00, 0xffff00, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
    }
};

const int ImageData::loading[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x00ff00, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
    },{
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x00ff00, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
    },{
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x00ff00, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
    }
};

const int ImageData::nightlight[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x0000bb, 0x000000, 0x000000,
        0x000000, 0x0000bb, 0x0000bb, 0x0000bb, 0x000000,
        0x0000bb, 0x0000bb, 0x0000bb, 0x0000bb, 0x0000bb,
        0x000000, 0x000000, 0xbbbb00, 0x000000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
    }
};

const int ImageData::question[][25] PROGMEM =
{
    {
        0x000000, 0x003300, 0x00ff00, 0x003300, 0x000000,
        0x000000, 0x00ff00, 0x000000, 0x00ff00, 0x000000,
        0x000000, 0x000000, 0x000000, 0x003300, 0x000000,
        0x000000, 0x000000, 0x003300, 0x000000, 0x000000,
        0x000000, 0x000000, 0x00ff00, 0x000000, 0x000000
    }
};

const int ImageData::sad[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0xff0000, 0x000000, 0xff0000, 0x000000,
        0x000033, 0x000000, 0x000000, 0x000000, 0x000033,
        0x000000, 0xff0000, 0xff0000, 0xff0000, 0x000000,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
    }
};

const int ImageData::thumbsUp[][25] PROGMEM =
{
    {
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
        0x000000, 0x000000, 0x666600, 0x000000, 0x000000,
        0x000000, 0x666600, 0x666600, 0x004400, 0x004400,
        0x000000, 0x666600, 0x666600, 0x004400, 0x004400,
        0x000000, 0x000000, 0x000000, 0x000000, 0x000000,
    }
};

const int ImageData::wifi[][25] PROGMEM =
{
    {
        0x08a1ec, 0x000000, 0x000000, 0x000000, 0x08a1ec,
        0x08a1ec, 0x000000, 0x000000, 0x000000, 0x08a1ec,
        0x08a1ec, 0x000000, 0x08a1ec, 0x000000, 0x08a1ec,
        0x08a1ec, 0x000000, 0x08a1ec, 0x000000, 0x08a1ec,
        0x000000, 0x08a1ec, 0x000000, 0x08a1ec, 0x000000,
    }
};
