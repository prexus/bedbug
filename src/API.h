#pragma once

#include "Images.h"

#include <AsyncTCP.h>

enum class RequestType {
    USER_INITIATED,
    AUTO_REPLY
};

class API {
    private:
        static AsyncClient *client;

        // Forward declarations for the callback functions
        static void onDataCallback(void *arg, AsyncClient *client, void *data, size_t len);
        static void onErrorCallback(void *arg, AsyncClient *client, int8_t error);
        static void onConnectCallback(void *arg, AsyncClient *client);

    public:
        static void sendImage(ImageType image);
        static void sendImage(ImageType image, RequestType isAutoReply);
};
