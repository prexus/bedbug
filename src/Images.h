#pragma once

enum class ImageType {
    CHECKMARK,
    COUNTER,
    DOT,
    ERROR,
    HAPPY,
    HEART,
    INDIFFERENT,
    LOADING,
    NIGHTLIGHT,
    QUESTION,
    SAD,
    THUMBS_UP,
    WIFI
};

struct Image
{
    const int id;
    const ImageType type;
    const char* label;
    const int (*frames)[25];
    const int totalFrames;
    const bool selectable;
};

class Images
{
    public:
        static const int getImageCount();

        static const Image* getImage(int imageId);
        static const Image* getImage(ImageType imageType);
        static const Image* getImageByIndex(int index);

    private:
        static const Image images[];
        static const int imageCount;
};
