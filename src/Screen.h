#pragma once

#include "Images.h"

class Screen
{
    public:
        // Draws a single image on the screen
        // with no surprising side effects.
        static void draw(ImageType type);
        static void draw(ImageType type, int brightness);

        // Draws a frame of the given image
        // and pauses 200 ms afterwards so
        // it can be called repeatedly.
        static void drawFrame(ImageType type);

        // Draws a splash screen that dismisses
        // after 3 seconds. It can also be
        // dismissed by clicking the screen.
        static void splash(ImageType type);

        // Idle screen has its own call to
        // check the options that can be
        // assigned on the settings page.
        static void idle();

        // Update can be called for the idle
        // screen in case depending values has
        // been changed. Ignored if not idling.
        static void update();

    private:
        static int currentFrame;
        static bool idling;

        static void drawHeartCounter(int brightness);

        static void drawFrame(ImageType type, int frame, int brightness);

        static void print(const int * image, int brightness);
        static void printFrame(const int image[][25], int &frameIndex, const int &frameCount);
};
