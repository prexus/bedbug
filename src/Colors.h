#include <Arduino.h>

class Colors {
    public:
        static unsigned int dim(unsigned int& color, signed char& pct);

    private:
        static void hexToHsv(size_t& hex, float& hue, float& saturation, float& value);
        static void hsvToHex(size_t& hex, float& hue, float& saturation, float& value);
};
