#pragma once

#include "Songs.h"

class Buzzer {
    private:
        static const int channel;

        static bool looping;
        static bool resting;

        static int noteIndex;
        static int actionTimer;

        static Song * currentSong;

        static void lastNoteReached();
        static void readyNextNote(int rest);
        static void startNote();
        static void stopNote();

    public:
        static void init();
        static void play(int songId);
        static void play(int songId, bool loop);
        static void stop();
        static void update();
};
