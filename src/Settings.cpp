#include <Arduino.h>
#include <ArduinoJson.h>
#include <Preferences.h>

#include "State.h"
#include "Settings.h"
#include "Images.h"

DynamicJsonDocument Settings::settings(1024);

void Settings::load() {
    Preferences preferences;

    preferences.begin("settings");

    if (!preferences.isKey("settings")) {
        JsonArray array = settings.createNestedArray("screens");

        auto default1 = Images::getImage(ImageType::HEART);
        auto default2 = Images::getImage(ImageType::QUESTION);
        auto default3 = Images::getImage(ImageType::SAD);

        array.add(default1->id);
        array.add(default2->id);
        array.add(default3->id);

        settings["target"] = "";
        settings["clickToDismiss"] = false;
        settings["autoRespondToHearts"] = false;
        settings["buzzerSongId"] = 0;
        settings["idleScreenMode"] = "default";
        settings["idleScreenBrightness"] = 100;

        String json;

        serializeJson(settings, json);

        preferences.putString("settings", json);
    } else {
        String json = preferences.getString("settings");

        // Deserializes into our global settings object
        // which is of course a little magical but in an
        // application of this size I think it's fine.
        deserializeJson(settings, json);
    }

    preferences.end();
}

void Settings::save(JsonObject jsonObject) {
    // This does feel a little bit like a hack. But assigning
    // body directly to settings gave all sorts of strange
    // pointer errors so the output json was gibberish.
    String jsonString;

    serializeJson(jsonObject, jsonString);
    deserializeJson(settings, jsonString);

    Preferences preferences;

    preferences.begin("settings");
    preferences.putString("settings", jsonString);
    preferences.end();
}

String Settings::getSettings() {
    String json;

    serializeJson(settings, json);

    return json;
}

int Settings::getScreenCount() {
    JsonArray screens = settings["screens"];
    int screenCount = screens.size();

    return screenCount;
}

int Settings::getScreen(int screenIndex) {
    JsonArray screens = settings["screens"];
    int imageIndex = screens[screenIndex];

    return imageIndex;
}

String Settings::getTarget() {
    return settings["target"];
}

int Settings::buzzerSongId() {
    return settings["buzzerSongId"];
}

bool Settings::clickToDismiss() {
    return settings["clickToDismiss"];
}

bool Settings::autoRespondToHearts() {
    return settings["autoRespondToHearts"];
}

String Settings::idleScreenMode() {
    return settings["idleScreenMode"];
}

int Settings::idleScreenBrightness() {
    return settings["idleScreenBrightness"];
}
