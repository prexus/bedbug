#include <Arduino.h>

#include "Timers.h"

int Timers::heartTimer = 0;
int Timers::resetTimer = 0;
int Timers::sendTimer = 0;

bool Timers::triggerHeartTimer() {
    if (Timers::heartTimer > 0 && millis() > Timers::heartTimer) {
        heartTimer = 0;

        return true;
    }

    return false;
}

bool Timers::triggerResetTimer() {
    if (Timers::resetTimer > 0 && millis() > Timers::resetTimer) {
        resetTimer = 0;

        return true;
    }

    return false;
}

bool Timers::triggerSendTimer() {
    if (Timers::sendTimer > 0 && millis() > Timers::sendTimer) {
        sendTimer = 0;

        return true;
    }

    return false;
}

void Timers::setHeartTimer(int ms) {
    heartTimer = millis() + ms;
}

void Timers::setResetTimer(int ms) {
    resetTimer = millis() + ms;
}

void Timers::setSendTimer(int ms) {
    sendTimer = millis() + ms;
}

void Timers::clearSendTimer() {
    sendTimer = 0;
}
