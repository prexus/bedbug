#pragma once

#include <Arduino.h>
#include <ArduinoJson.h>

class Settings
{
    public:
        static void load();
        static void save(JsonObject jsonObject);

        static String getSettings();

        static int getScreenCount();
        static int getScreen(int index);

        static String getTarget();

        static int buzzerSongId();
        static bool clickToDismiss();
        static bool autoRespondToHearts();

        static String idleScreenMode();
        static int idleScreenBrightness();

    private:
        static DynamicJsonDocument settings;
};
