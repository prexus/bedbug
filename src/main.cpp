#include <Arduino.h>
#include <M5Atom.h>
#include <WifiManager.h>

#include "BedBug.h"

void setup() {
    // Initializes serial and display
    M5.begin(true, false, true);

    // Initializes with WiFi screen
    BedBug::initialize();

    // Attempts to connect to Wifi using stored credentials
    bool connected = WifiManager.connectToWifi();

    if (!connected) {
        // If unable to connect on boot, the management server is started so
        // new credentials can be set using the webinterface.
        WifiManager.startManagementServer("BEDBUG-WIFI");
    } else {
        // Starts up in idle mode so we know it is time to sleep
        // and we know that wifi is now properly connected.
        BedBug::start();
    }
}

void loop() {
    // Continously checks if we are connected on WIFI. The ESP will restart
    // if connection is lost. This will make the ESP restart. Known isssue here
    // is if Wifi is unabl to reconnect (eventhough it should work) the system
    // will boot up with the wifi manager. Should only do that on complete power cycles.
    WifiManager.check();

    // Listens for clicks and then cycles through all the supported modes.
    // Once button has not been clicked for a little while, it will send a
    // request with the selected mode with to configured API.
    BedBug::check();
}
