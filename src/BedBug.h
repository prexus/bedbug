#pragma once

#include "Images.h"

class BedBug {
    private:
        static bool started;

        static void onButtonPressed();

        static void transferSelectedImage();
        static void transferImage(ImageType image);

        static ImageType getCurrentImage();
        static void showNextImage();

        static void initLoadingScreen();
        static void showLoadingScreen();

    public:
        static void initialize();
        static void start();
        static void check();
};
