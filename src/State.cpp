#include "State.h"
#include "Screen.h"
#include "Images.h"

int State::clickCycle = 0;
int State::heartCounter = 0;

bool State::loading = false;
bool State::success = false;

bool State::clearOnNextClick = false;

void State::reset() {
    clickCycle = 0;
    heartCounter = 0;

    clearOnNextClick = false;

    Screen::idle();
}
