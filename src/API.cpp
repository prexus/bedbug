#include "API.h"
#include "Images.h"
#include "State.h"
#include "Settings.h"

AsyncClient *API::client = nullptr;

void API::sendImage(ImageType image) {
    sendImage(image, RequestType::USER_INITIATED);
}

void API::sendImage(ImageType type, RequestType requestType) {
    if (!client) {
        client = new AsyncClient();
    }

    // Reset the client if it's in use or closed
    if (client->connected()) {
        client->close();
    }

    auto image = Images::getImage(type);
    String target = Settings::getTarget();
    String url = "http://" + target + "/draw/?id=" + image->id;

    if (requestType == RequestType::AUTO_REPLY) {
        url += "&ignoreAutoReply=true";
    }

    Serial.printf("Query URL: %s\n", url.c_str());

    // Attach the callbacks if they haven't been attached yet
    client->onConnect(onConnectCallback, new String(url));
    client->onData(onDataCallback, nullptr);
    client->onError(onErrorCallback, nullptr);

    client->connect(Settings::getTarget().c_str(), 80);

    State::loading = true;
}

void API::onConnectCallback(void *arg, AsyncClient *client) {
    String *url = (String *)arg;

    Serial.println("Connected, sending request");

    String request = "POST " + *url + " HTTP/1.1\r\n" +
                     "Host: " + Settings::getTarget() + "\r\n" +
                     "Connection: close\r\n\r\n";

    client->write(request.c_str());

    delete url;
}

// Extracted onData callback function
void API::onDataCallback(void *arg, AsyncClient *client, void *data, size_t len) {
    Serial.println("Response received");

    // Optionally, process the response data
    // Example: String response = String((char*)data).substring(0, len);

    State::success = true;
    State::loading = false;

    client->close();
    delete client;  // Clean up after the request is done
}

// Extracted onError callback function
void API::onErrorCallback(void *arg, AsyncClient *client, int8_t error) {
    Serial.printf("Connect Error: %d\n", error);

    State::success = false;
    State::loading = false;

    delete client;  // Clean up after the error
}
