#pragma once

class Timers {
    public:
        static int heartTimer;
        static int resetTimer;
        static int sendTimer;

        static bool triggerHeartTimer();
        static bool triggerResetTimer();
        static bool triggerSendTimer();

        static void setHeartTimer(int ms);
        static void setResetTimer(int ms);
        static void setSendTimer(int ms);

        static void clearSendTimer();
};
